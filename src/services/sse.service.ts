import {Injectable, NgZone} from '@angular/core';
import {Observable} from "rxjs";
import {KeycloakService} from "keycloak-angular";
import {EventSourcePolyfill} from "event-source-polyfill";

@Injectable({
  providedIn: 'root'
})
export class SseService {

  constructor(private _zone: NgZone, private _keycloakService: KeycloakService) {
  }

  getServerSentEvent(url: string): Observable<any> {

    return new Observable(observer => {

      let eventSource: EventSourcePolyfill;

      this._keycloakService.getToken().then(token => {

        eventSource = new EventSourcePolyfill(url, {headers: {Authorization: `Bearer ${token}`}})

        eventSource.onmessage = event => {
          this._zone.run(() => {
            observer.next(JSON.parse(event.data));
          });
        };

        eventSource.onerror = (error: any) => {
          this._zone.run(() => {
            observer.error(error);
          });
        }
      })

      return () => eventSource.close()

    })
  }
}

import {Injectable} from '@angular/core';
import {SseService} from "./sse.service";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Email} from "../entities/email";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private _sseService: SseService, private _httpClient : HttpClient) {}

  public getLoggedInUsersReceivedEmails(): Observable<Email> {
    return this._sseService.getServerSentEvent(
      '/api/email/listLoggedInUsersReceivedEmails'
    );
  }
  public getLoggedInUsersSentEmails(): Observable<Email> {
    return this._sseService.getServerSentEvent(
      '/api/email/listLoggedInUsersSentEmails'
    );
  }
  public sendEmail(email: Email): Observable<Email> {
    return this._httpClient.post<Email>('/api/email/sendMail', email);
  }
  public listEmailsByConversationId(conversationId: string): Observable<Email> {
    return this._sseService.getServerSentEvent(
      '/api/email/listEmailsByConversationId/' + conversationId
    );
  }

}

import {APP_INITIALIZER, LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {KeycloakAngularModule, KeycloakService} from 'keycloak-angular';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {EmailListComponent} from './email-list/email-list.component';
import {HttpClientModule} from "@angular/common/http";
import {environment} from "../environments/environment";
import {MatTableModule} from "@angular/material/table";
import {LoginComponent} from './login/login.component';
import {MatGridListModule} from "@angular/material/grid-list";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatListModule} from "@angular/material/list";
import {MatButtonModule} from "@angular/material/button";
import {SendEmailComponent} from './send-email/send-email.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatChipsModule} from "@angular/material/chips";
import {MatIconModule} from "@angular/material/icon";
import {AngularEditorModule} from "@kolkov/angular-editor";
import { EmailChainDialogComponent } from './email-list/email-chain-dialog/email-chain-dialog.component';
import {MatTabsModule} from "@angular/material/tabs";
import {MatDialogModule} from "@angular/material/dialog";
import {MatCardModule} from "@angular/material/card";
import localeHu from '@angular/common/locales/hu';
import {registerLocaleData} from "@angular/common";
import {MatSnackBarModule} from "@angular/material/snack-bar";
registerLocaleData(localeHu)

function initializeKeycloak(keycloak: KeycloakService) {
  return () =>
    keycloak.init({
      config: {
        url: environment.keycloakUrl,
        realm: 'oauth2_emszhw-szakdolgozat',
        clientId: 'keycloak-user',
      },
      initOptions: {
        onLoad: 'check-sso',
        silentCheckSsoRedirectUri: window.location.origin + '/assets/silent-check-sso.html',
      },
    });
}

@NgModule({
  declarations: [
    AppComponent,
    EmailListComponent,
    LoginComponent,
    SendEmailComponent,
    EmailChainDialogComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    KeycloakAngularModule,
    HttpClientModule,
    MatTableModule,
    MatGridListModule,
    FlexLayoutModule,
    MatListModule,
    MatButtonModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatChipsModule,
    MatIconModule,
    AngularEditorModule,
    MatTabsModule,
    MatDialogModule,
    MatCardModule,
    FormsModule,
    MatSnackBarModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService]
    },
    {
      provide: LOCALE_ID,
      useValue: 'hu-HU'
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

import {Component, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {IPageStructure} from "../interfaces/page-structure.interface";
import {Email} from "../entities/email";
import {ApiService} from "../services/api.service";
import {MatTableDataSource} from "@angular/material/table";
import {Subscription} from "rxjs";
import {MatTabGroup} from "@angular/material/tabs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, OnDestroy {
  @HostListener('window:beforeunload', ['$event'])
  beforeUnloadHandler() {
    this.ngOnDestroy()
  }

  @ViewChild('tabs') tabs : MatTabGroup

  private _isLoggedIn: boolean = false;
  get isLoggedIn(): boolean {
    return this._isLoggedIn;
  }

  private _receivedEmailDatasource: MatTableDataSource<Email> = new MatTableDataSource<Email>();
  get receivedEmailDatasource(): MatTableDataSource<Email> {
    return this._receivedEmailDatasource;
  }

  private _sentEmailDatasource: MatTableDataSource<Email> = new MatTableDataSource<Email>();
  get sentEmailDatasource(): MatTableDataSource<Email> {
    return this._sentEmailDatasource;
  }

  private _pageStructure: IPageStructure[] = [
    {cols: 1, rows: 1},
    {cols: 9, rows: 1}
  ]
  get pageStructure(): IPageStructure[] {
    return this._pageStructure;
  }

  private _subscriptions: Subscription[] = []

  private _emailChain: Email[] = []
  get emailChain(): Email[] {
    return this._emailChain;
  }

  private _predefinedEmail : Email
  get predefinedEmail(): Email {
    return this._predefinedEmail;
  }
  set predefinedEmail(value: Email) {
    this._predefinedEmail = value;
  }

  constructor(private _apiService: ApiService) {
  }

  ngOnInit() {
    this._initDatasources()
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach(subscription => subscription.unsubscribe())
  }

  private _initDatasources() {
    if (this._isLoggedIn) {

      this._subscriptions.push(this._apiService.getLoggedInUsersReceivedEmails().subscribe(email => {
          this._receivedEmailDatasource.data = this._filterEmails(
            this._receivedEmailDatasource.data,
            email
          )
        })
      )

      this._subscriptions.push(this._apiService.getLoggedInUsersSentEmails().subscribe(email => {
          this._sentEmailDatasource.data = this._filterEmails(
            this._sentEmailDatasource.data,
            email
          )
        })
      )

    }
  }

  private _filterEmails(existingData: Email[], newEmail: Email): Email[] {
    const sameConversationIdEmails = existingData.filter(email => email.conversationId === newEmail.conversationId)


    if (sameConversationIdEmails.length === 0) {

      existingData.push(newEmail)

    } else {

      if (sameConversationIdEmails.filter(e => new Date(e.sentDate) > new Date(newEmail.sentDate)).length === 0) {

        sameConversationIdEmails.forEach(p => {
          existingData.splice(existingData.indexOf(p), 1)
        })

        existingData.push(newEmail)
      }
    }

    existingData.sort((a, b) => new Date(b.sentDate).getTime() - new Date(a.sentDate).getTime())


    return existingData
  }

  loggedInStateChanged(newState: boolean) {
    this._isLoggedIn = newState;
    this._initDatasources()
  }

  replyToAChain(email : Email){
    this.tabs.selectedIndex = 3

    this._predefinedEmail = email
  }

}

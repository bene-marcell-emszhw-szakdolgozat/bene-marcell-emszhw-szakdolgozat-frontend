import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {KeycloakProfile} from "keycloak-js";
import {KeycloakService} from "keycloak-angular";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  get isLoggedIn(): boolean {
    return this._isLoggedIn;
  }

  @Output()
  isLoggedInEvent = new EventEmitter<boolean>();

  private _isLoggedIn: boolean = false;
  private _userProfile: KeycloakProfile | null = null;

  constructor(private readonly keycloak: KeycloakService) {
  }

  public async ngOnInit() {
    this._isLoggedIn = await this.keycloak.isLoggedIn();

    // Ha úgy töltődik be az oldal, hogy be van loginolva egy felhasználó
    if (this._isLoggedIn) {

      // Akkor közölje a parent component-tel is
      this.isLoggedInEvent.emit(true);
      this._userProfile = await this.keycloak.loadUserProfile();

    } else {

      // Ha meg nincs beloginolva, akkor ezt is közölje a parent component-tel
      this.isLoggedInEvent.emit(false);

    }
  }

  public login() {
    this.keycloak.login();
  }

  public logout() {
    this.keycloak.logout();
  }

}

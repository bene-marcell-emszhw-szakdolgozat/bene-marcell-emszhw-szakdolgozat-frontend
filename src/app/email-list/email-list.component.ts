import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Email} from "../../entities/email";
import {MatTableDataSource} from "@angular/material/table";
import {MatDialog} from "@angular/material/dialog";
import {EmailChainDialogComponent} from "./email-chain-dialog/email-chain-dialog.component";
import {ApiService} from "../../services/api.service";

@Component({
  selector: 'app-email-list',
  templateUrl: './email-list.component.html',
  styleUrls: ['./email-list.component.css']
})
export class EmailListComponent implements OnInit {
  get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  get emailDatasource(): MatTableDataSource<Email> {
    return this._emailDatasource;
  }

  @Input()
  set emailDatasource(value: MatTableDataSource<Email>) {
    this._emailDatasource = value;
  }

  @Output()
  emailChainDialogClosed = new EventEmitter<Email>()

  private _emailDatasource: MatTableDataSource<Email> = new MatTableDataSource<Email>()
  private _displayedColumns: string[] = [
    'from',
    'subject',
    'sentDate'
  ];

  constructor(private _dialog: MatDialog, private _apiService: ApiService) {
  }

  ngOnInit(): void {}

  clickOnRow(row: Email) {
    const dialogRef = this._dialog.open(EmailChainDialogComponent, {
      data: this._apiService.listEmailsByConversationId(row.conversationId), width: '98vw',
      maxWidth: '98vw'
    });

    dialogRef.afterClosed().subscribe(returnValue => {
      if (returnValue) {

        this.emailChainDialogClosed.emit(returnValue)

      }
    })
  }
}

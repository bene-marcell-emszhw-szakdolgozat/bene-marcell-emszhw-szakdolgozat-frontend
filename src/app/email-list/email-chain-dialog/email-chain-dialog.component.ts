import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Email} from "../../../entities/email";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Observable, Subscription} from "rxjs";
import {KeycloakService} from "keycloak-angular";
import {ApiService} from "../../../services/api.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'email-chain-dialog',
  templateUrl: './email-chain-dialog.component.html',
  styleUrls: ['./email-chain-dialog.component.css']
})
export class EmailChainDialogComponent implements OnInit, OnDestroy {
  constructor(
    @Inject(MAT_DIALOG_DATA) private _emailsObservable: Observable<Email>,
    private _keycloakService: KeycloakService,
    private _apiService: ApiService,
    private _dialogRef: MatDialogRef<EmailChainDialogComponent>
  ) {
  }

  private _form: FormGroup = new FormGroup({
    textArea: new FormControl(null, [Validators.required])
  })
  get form(): FormGroup {
    return this._form;
  }

  private _emails: Email[] = []
  get emails(): Email[] {
    return this._emails;
  }

  private _subscription: Subscription;

  get loggedInUserName(): string {
    return this._keycloakService.getUsername()
  }

  get firstEmailsSubject(): string {
    return this._emails.sort((a, b) => new Date(a.sentDate).getTime() - new Date(b.sentDate).getTime())[0].subject
  }

  private _participants(): Set<string> {
    const returnSet: Set<string> = new Set<string>();

    this._emails
      .filter(email => email.from !== this._keycloakService.getUsername() || !email.to.includes(this._keycloakService.getUsername()))
      .forEach(email => {
        if (email.from !== this._keycloakService.getUsername()) {
          returnSet.add(email.from)
        } else {
          email.to.forEach(emailTo => {
            returnSet.add(emailTo)
          })
        }
      })

    return returnSet;

  }

  private _replyEmail(): Email {
    const lastEmail = this._emails[this._emails.length - 1]

    return new Email(
      null,
      null,
      [lastEmail.id],
      lastEmail.replyReferences.concat(lastEmail.id),
      lastEmail.subject,
      null,
      [...this._participants()],
      null,
      null,
      null,
      null,
      null,
      null
    )
  }

  ngOnInit(): void {
    this._subscription = this._emailsObservable.subscribe(next => this._emails.push(next))
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  fastReply() {

    const fastReply = this._replyEmail()
    fastReply.htmlContent = this._form.get("textArea")!.value

    this._apiService.sendEmail(
      fastReply
    ).subscribe(() => {
      this._form.reset()
    })



  }

  reply() {
    this._dialogRef.close(this._replyEmail())
  }
}

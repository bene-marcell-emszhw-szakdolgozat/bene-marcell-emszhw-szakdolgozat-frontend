import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";
import {ApiService} from "../../services/api.service";
import {COMMA, ENTER} from "@angular/cdk/keycodes";
import {MatChipInputEvent} from "@angular/material/chips";
import {AngularEditorConfig} from "@kolkov/angular-editor";
import {Email} from "../../entities/email";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-send-email',
  templateUrl: './send-email.component.html',
  styleUrls: ['./send-email.component.css']
})
export class SendEmailComponent implements OnInit, OnChanges {

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      {class: 'arial', name: 'Arial'},
      {class: 'times-new-roman', name: 'Times New Roman'},
      {class: 'calibri', name: 'Calibri'},
      {class: 'comic-sans-ms', name: 'Comic Sans MS'}
    ],
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      ['bold', 'italic'],
      ['fontSize']
    ]
  };

  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;


  private _preDefinedEmail: Email
  get preDefinedEmail(): Email {
    return this._preDefinedEmail;
  }

  @Input()
  set preDefinedEmail(value: Email | undefined) {
    if (value) {
      this._preDefinedEmail = value;
    } else {
      this.reset()
    }

  }

  private _form: FormGroup
  get form(): FormGroup {
    return this._form;
  }


  constructor(
    private _apiService: ApiService,
    private _snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    this._form = new FormGroup({
      to: new FormControl(this._preDefinedEmail.to, [Validators.required]), // TODO email array validator
      cc: new FormControl(this._preDefinedEmail.cc, Validators.email),
      bcc: new FormControl(this._preDefinedEmail.bcc, Validators.email),
      subject: new FormControl(this._preDefinedEmail.subject),
      htmlContent: new FormControl(this._preDefinedEmail.htmlContent)
    })
  }

  submit() {

    this._preDefinedEmail.to = this._form.get("to")!.value
    this._preDefinedEmail.cc = this._form.get("cc")!.value
    this._preDefinedEmail.bcc = this._form.get("bcc")!.value
    this._preDefinedEmail.subject = this._form.get("subject")!.value
    this._preDefinedEmail.htmlContent = this._form.get("htmlContent")!.value

    this._apiService.sendEmail(this._preDefinedEmail).subscribe(() => {
      this._form.reset()
      this._snackBar.open("The email has been sent!",undefined, {duration: 3000})
    })
  }

  add(event: MatChipInputEvent, formControl: AbstractControl): void {

    // Ha az input, amit bevittünk valid, és nem üres char
    if ((event.value || '').trim()) {

      // Ha ennek a FormControl-nak a jelenlegi értéke valid (nem null / undefined)
      // ÉS nem tartalmazza a lista már ezt a string-et
      if (formControl.value && !formControl.value.includes(event.value)) {

        // Akkor be tudjuk állítani, hogy az előző értékek + a jelenleg bevitt valid érték
        formControl.setValue([...formControl.value, event.value])
      } else {

        // Különben csak a valid bevitt értéket álíltsa be
        formControl.setValue([event.value])
      }

      formControl.updateValueAndValidity()

      if (event.chipInput) {
        event.chipInput!.inputElement.value = ''
      }

    }

  }

  remove(address: string, formControl: AbstractControl): void {

    // Ha ennek a FormControl-nak a jelenlegi értéke valid (nem null / undefined) ((indexOf miatt))
    if (formControl.value) {

      const indexOfAddress = formControl.value.indexOf(address)

      if (indexOfAddress >= 0) {
        formControl.value.splice(indexOfAddress, 1)
        formControl.updateValueAndValidity()
      }

    }
  }

  reset() {
    this._preDefinedEmail = new Email(
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    )
  }
}

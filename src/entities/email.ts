import {Attachment} from "./attachment";

export class Email {
  public id: string;
  public sentDate: Date;
  public inReplyTo: string[]
  public replyReferences: string[]
  public subject: string;
  public from: string;
  public to: string[];
  public cc: string[];
  public bcc: string[];
  public plainContent: string;
  public htmlContent: string;
  public attachments: Attachment[];
  public conversationId: string;


  constructor(id: string | null, sentDate: Date | null, inReplyTo: string[] | null, replyReferences: string[] | null, subject: string | null, from: string | null, to: string[] | null, cc: string[] | null, bcc: string[] | null, plainContent: string | null, htmlContent: string | null, attachments: Attachment[] | null, conversationId: string | null) {
    if (id) this.id = id
    if (sentDate) this.sentDate = sentDate;
    if (inReplyTo) this.inReplyTo = inReplyTo;
    if (replyReferences) this.replyReferences = replyReferences;
    if (subject) this.subject = subject;
    if (from) this.from = from;
    if (to) this.to = to;
    if (cc) this.cc = cc;
    if (bcc) this.bcc = bcc;
    if (plainContent) this.plainContent = plainContent;
    if (htmlContent) this.htmlContent = htmlContent;
    if (attachments) this.attachments = attachments;
    if (conversationId) this.conversationId = conversationId;
  }
}

export class Attachment {
  public data: Buffer;
  public contentType: string;
  public name: string;

  constructor(data: Buffer, contentType: string, name: string) {
    this.data = data;
    this.contentType = contentType;
    this.name = name;
  }
}

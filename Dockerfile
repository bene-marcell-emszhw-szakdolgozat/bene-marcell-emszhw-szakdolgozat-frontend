FROM node:16-slim as frontend

WORKDIR /usr/local/app

COPY ./ /usr/local/app/

RUN npm install
RUN npm run build

ENTRYPOINT ["/bin/bash"]
